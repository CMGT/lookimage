<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Picture;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        // Equivalent : SELECT * FROM picture
        // findAll() retourne tous les résultats trouvés dans la table "pictures"
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll(); // démarre doctrine pour travailler avec la BDD, puis appel de la class Picture, findAll() afin de récup toutes les données de la table

        // Equivalent : SELECT * FROM categories
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        // Utilisation du paginator
        $pictures = $paginator->paginate(
            $resultsPictures, // requête contenant les images à paginer 
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            15 // Nombre de résultats par page
        );

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
        ]);
    }


    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, PaginatorInterface $paginator, Request $request)
    {
        // dd veut dire "dump data", équivalent de var_dump()
        // ATTENTION, il arrete tout le chargement de la page pour afficher l'information contenue
        // dd($id);

        
        // dump(), équivalent de var_dump()
        // Laisse la page se charger complètement et affiche le contenu du "dump" dans la debug bar en bas

        // find() sélectionne UN SEUL enregistrement selon son ID
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id); // ATTENTION find() ne fonctionne qu'avec l'id 
        
        // Si la catégorie est introuvable, alors on génère une erreur 404
        if (!$category) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        // Pagination pour les images liées à la categorie
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $category->getPictures(), // getPictures est le getter dans l'entity qui récupère toutes les images liées à la categorie
            $page === 0 ? 1 : $page,
            5
        );

        // Si la catégorie existe, nous envoyons les données à la vue
        return $this->render('home/picturesByCategory.html.twig', [
            'category' => $category,
            'pictures' => $pictures
        ]);
    }



    /**
     * "requirements" permet de valider le type de la donnée passée en paramètre, ici id = nbr décimale entier, à plusieurs décimales (+)
     * @Route("/picture/{id}", name="picture_by_id"), requirements={"id"="\d+"})
     */
    public function pictureById($id, PaginatorInterface $paginator, Request $request)
    {
        

        // find() sélectionne UN SEUL enregistrement selon son ID
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if (!$picture) {
            throw $this->createNotFoundException('L\'image n\'existe pas');
        }


        // Pagination pour les images 
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategory()->getPictures(),
            $page === 0 ? 1 : $page,
            5
        );

        
        // Si l'image existe, nous envoyons les données à la vue
        return $this->render('home/pictureById.html.twig', [
            'picture' => $picture,
            'photos' => $photos
            
        ]);  
    }



    


}
