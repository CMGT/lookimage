<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use App\Form\EditPictureType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        /**
         * $this->getUser() est l'équivalent de "app.user" en twig
         * Par exemple, si je veux le prénom : $this->getUser()->getFirstname()
         */

        // Méthode 1
        /* 
        $page = $request->query->getInt('page', 1);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([ // findBy() retourne 0 ou plusieurs résultats
            'user' => $this->getUser()
        ]);
        */

        // Méthode 2
        $page = $request->query->getInt('page', 1);

        $pictures = $paginator->paginate(
            $this->getUser()->getPictures(), // méthode 2
            $page === 0 ? 1 : $page,
            20
        );


        return $this->render('account/index.html.twig', [
            'pictures' => $pictures
        ]);
    }




    /**
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request)
    {
        // Instancie l'entité "Picture"
        $picture = new Picture();

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formUpload = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'new_picture' // voir UploadPictureType.php => new_picture selectionne les contraintes utilisées à ce form
        ]);
        $formUpload->handleRequest($request); // handleRequest sert à hydrater avec les info du form dès l'envoi du formulaire

        // Vérifie si le formulaire est envoyé et valide
        if ($formUpload->isSubmitted() && $formUpload->isValid()) {

            // Ajoute la date du jour
            $picture->setCreatedAt(new \DateTimeImmutable());

            // Passe l'utilisateur actuellement connecté à notre setter
            $picture->setUser($this->getUser());

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush(); // flush insère toutes les données contenues dans notre objet Picture dans la table Picture

            // Création d'un message flash
            $this->addFlash('success', 'Merci pour votre partage !');

            // Redirection vers la page d'accueil du profil
            return $this->redirectToRoute('account');
        }

        // Si vous voulez capter toutes les erreurs d'un formulaire pour les afficher
        // où vous le souhaitez.
        //if ($formUpload->isSubmitted() && !$formUpload->isValid()) { // à faire boucler dans notre vue
            //$errors = $formUpload->getErrors(true);
        //}




        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView()
        ]);
    }

    /**
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request)
    { 
        // Sélectionner un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur404
        // ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
            
        }

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formEdit = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture' // voir UploadPictureType.php => selectionne les contraintes utilisées à ce form
        ]);
        $formEdit->handleRequest($request); // handleRequest sert à hydrater avec les info du form dès l'envoi du formulaire

        // Vérifie si le formulaire est envoyé et valide
        if ($formEdit->isSubmitted() && $formEdit->isValid()) {

            // Ajoute la date du jour
            $picture->setUpdatedAt(new \DateTimeImmutable());

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush(); // flush insère toutes les données contenues dans notre objet Picture dans la table Picture

            // Création d'un message flash
            $this->addFlash('success', 'Modification effectuée avec succès !');

            // Redirection vers la page d'accueil du profil
            return $this->redirectToRoute('account');
        }


        return $this->render('account/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]);
    }

    /**
     * @Route("/account/delete/picture/{id}", name="account_delete_picture")
     */
    public function delete($id)
    {
        // Sélectionner un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur404
        // ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
            
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo a bien été supprimée !');

        return $this->redirectToRoute('account');
    }
}
    