<?php

namespace App\Controller;

use App\Entity\Category;
use Knp\Component\Pager\PaginatorInterface;
use App\Form\ManageCategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {

        // Equivalent du SELECT * FROM category
        $resultsCategories = $this->getDoctrine()->getRepository(Category::class)->findAll();
            
        // Utilisation du paginator
        $categories = $paginator->paginate(
            $resultsCategories, // requête contenant les catégories à paginer 
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );


        return $this->render('admin/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/new/category", name="admin_new_category")
     */
    public function new(Request $request)
    {
        // Instancie l'entité Category
        $category = new Category();

        // Premier paramètre : le formulaire dont on a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formCreateCategory = $this->createForm(ManageCategoryType::class, $category);
        $formCreateCategory->handleRequest($request); 

        // Vérifie si le formulaire est envoyé et valide
        if ($formCreateCategory->isSubmitted() && $formCreateCategory->isValid()) {
            // Transforme automatiquement le nom de la catégorie en slug (=> voir CategoryFixtures.php)
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Nouvelle catégorie créée avec succès !');

            // Redirection vers la page d'accueil de l'admin
            return $this->redirectToRoute('admin');
        
        }

        return $this->render('admin/new.html.twig', [
            'formCreateCategory' => $formCreateCategory->createView()
        ]);

    }

    /**
     * @Route("/admin/edit/category{id}", name="admin_edit_category")
     */
    public function edit($id, Request $request)
    {
        // Sélectionner un enregistrement en BDD
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas :('); // Erreur 404 - Ressource non trouvée
            
            
        }


    }
    

    
}
