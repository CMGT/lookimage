<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true, // champ obligatoire => ajoute attribut required dans le champ (utile uniquement dans l'html)
                'label' => 'Prénom', // affiche Prénom en label à la place de firstname par défaut // si pas de label il faudra faire 'label' => false
                'attr' => [
                     'placeholder' => 'Votre prénom' // permet d'insérer un placeholder dans l'input 
                    // 'class' = 'nameClassCss deuxièmeClassCss' // permet d'ajouter des classes css au champ input
                ],
                'constraints' => [ // Contraintes de validation => vérification des données côté serveur
                    new NotBlank([ // permet d'insérer une contrainte si champ input vide 
                        'message' => 'Veuillez saisir un prénom' // génère un message d'erreur si champ vide
                    ])

                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Votre nom'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir votre nom'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email',
                'attr' => [
                    'placeholder' => 'Votre email'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir votre adresse email'
                    ]),
                    new Email([
                        'message' => 'Votre adresse email est invalide'
                    ])
                ]

            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false, // spécifie que ce champ n'est pas relié à l'entité en cours => ne sera pas inclu dans la BDD
                'required' => true,
                'label' => 'J\'accepte les conditions de confidentialité relatives à l\'utilisation de mes données personnelles.',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions de confidentialité relatives à l\'utilisation vos données personnelles',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmation du mot de passe'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Votre mot de passe'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir {{ limit }} caractères minimum',
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
