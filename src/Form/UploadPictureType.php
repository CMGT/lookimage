<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;




class UploadPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Sélectionner une photo',
                // Options de VichImageType
                'allow_delete' => false, // supprimer lien suppression
                'download_uri' => false, // supprimer lien de téléchargement
                'imagine_pattern' => 'thumbnail', // affichage de l'image téléchargée avec filtre Liip Imagine créé 
                // ---
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir une photo', // Message si champ vide
                        'groups' => ['new_picture'] // la contrainte NotBlank fait partie du group new_picture => permet d'utiliser ce même formulaire pour editer et donc utiliser cette contrainte uniquement pour new_picture et pas edit picture
                    ]),
                    new Image([
                        'maxSize' => '2M', // = 2Mo max
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 2Mo',
                        'mimeTypes' => [
                            'image/gif', 'image/png', 'image/jpeg', 'image/webp'
                        ],
                        'mimeTypesMessage' => 'Cette image est invalide',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]

            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Description de la photo',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une description',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('tags', TextType::class, [
                'required' => true,
                'label' => 'Tags',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un ou plusieurs tags',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            // Génère un menu déroulant contenant les données de la table "categorie"
            ->add('category', EntityType::class, [
                'required' => true,
                'label' => 'Catégorie de la photo',
                'class' => Category::class, 
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
