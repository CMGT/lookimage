<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger; 
use Faker;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Instancier Faker pour l'utiliser
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for() pour choisir le nombre d'éléments allant en BDD
        for($i = 0; $i <= 10; $i++) {
            $category = new Category();
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));
            
            // Enregistre l'objet dans une référence
            // On pourra utiliser les objets enregistrés dans une autre fixture
            // afin de pouvoir effectuer des relations de table (ex: category_id pour Picture)
            // ---
            // premier paramètre est un nom qui se doit être UNIQUE
            // second paramètre est l'objet qui sera lié à ce nom
            $this->addReference('category_'. $i, $category); // addReference(nom qui se doit être unique, objet qui sera lié à ce nom)



            // Garde de côté en attendant l'execution des requêtes 
            $manager->persist($category);

        }
        
        

        $manager->flush();
    }
}
