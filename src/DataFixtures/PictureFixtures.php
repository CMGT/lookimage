<?php

namespace App\DataFixtures;
use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger; 
use Mmo\Faker\PicsumProvider;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Permet de dire à notre fixtures, si elle dépend d'autres fixtures
     * afin de ne pas avoir d'erreur lors de l'utilisation des "getReference()"
     * L'ordre des fixtures ne sera plus par ordre alphabétique mais par ordre de dépendances
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        // Instancie Faker pour l'utiliser 
        $faker = Faker\Factory::create('fr_FR');

        // On ajoute le bundle "Mmo Faker-image" à Faker
        $faker->addProvider(new PicsumProvider($faker));

        // Création d'une boucle for() pour choisir le nombre d'éléments allant en BDD
        for($i = 0; $i <= 100; $i++) {

            // Spécifier le chemin du dossier d'upload
            // Les deux autres paramètres sont la hauteur aléatoire et la largeur aléatoire de l'image à récupérer 
            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152, 2312), random_int(864, 1736));

            // Récupération d'une référence aléatoirement
            // On récupère un objetde l'entité Category généré dans le fichier CategoryFixtures
            // grâce au nom choisi lors de l'enregistrement dans les références
            $category = $this->getReference('category_'. random_int(0, 10)); // random_int(0, 10) génère un nombre aléatoire entre 0 et 10 
            // (10 min et 10 max car il y a 10 categories rentrées donc $i min 0 et $i max 10 dans la boucle du tableau CategoryFixtures)


            // Récupère une référence utilisateur aléatoirement
            $user = $this->getReference('user_'. random_int(0, 10));

            $picture = new Picture();
            $picture->setDescription($faker->sentence(6)); // genere une phrase en lorem de 6 mots pour chaque ligne en BDD
            $picture->setTags($faker->word); // genere un mot aléatoire pour chaque ligne en BDD
            $picture->setCreatedAt($faker->dateTimeBetween('-4 years')); // genere une date aléatoire d'il y a 4 ans à aujourd'hui pour chaque ligne en BDD
            $picture->setUpdatedAt($faker->dateTimeBetween('-3 years')); // genere une date aléatoire d'il y a 3 ans à aujourd'hui pour chaque ligne en BDD 
            $picture->setCategory($category); // genere $category qui comprend $this->getReference('category_'. random_int(0, 10));
            $picture->setUser($user);

            // Gestion de l'image
            // A insérer en haut de page : use Symfony\Component\HttpFoundation\File\File;
            // str_replace() permet de cherche un morceau de caractères dans une chaine de caractères, et nous pouvons le remplacer par ce que l'on souhaite
            // 1er argument: chaîne à rechercher
            // 2eme argument: par quoi remplacer le 1er argument
            // 3eme argument: où effectuer la recherche?
            $picture->setImageFile(new File($image));
            $picture->setImage(str_replace('./public/uploads/images/photos\\', '', $image)); // = str_replace remplace './public/uploads/images/photos\\' par '' rien, dans $image

            // Garde de côté en attendant l'execution des requêtes 
            $manager->persist($picture);

        }

        $manager->flush();
    }
}
