/**
 * deleteModal.js
 */

// Chargement du DOM
window.onload = function() {

    // Ecouteur d'évènement sur les boutons de suppressions
    document.querySelectorAll('.confirm-delete').forEach((button) => {
        button.addEventListener('click', function() {
            let id = this.dataset.id;
            let href = `/account/delete/picture/${id}`;

            // Transmet l'id de l'url dans le href du bouton de la modal
            let modal = document.querySelector('#exampleModalCenter');
            let button = modal.querySelector('.btn-danger');
            button.href = href;

            var myModal = new bootstrap.Modal(modal);
            myModal.show();
        })
    });

}